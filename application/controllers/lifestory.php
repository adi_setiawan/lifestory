<?php 
class Lifestory extends CI_Controller{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('model_lifestory');
                $this->load->model('model_lifestoryide');
                $this->load->library('form_validation');
	}
	
	function home()
	{
		$data['title'] = 'Home | Lifebuoy 10 Tahun Berbagi Sehat';
		$this->load->view('head',$data);
		$this->load->view('home');
	}
	
	function timeline()
	{
		$data['title'] = 'Perjalanan 10 Tahun | Lifebuoy 10 Tahun Berbagi Sehat';
		$data['head'] = $this->load->view('head',$data);
		$this->load->view('timeline');
	}
	
	function pencapaian()
	{
		$data['title'] = 'Pencapaian Posyandu | Lifebuoy 10 Tahun Berbagi Sehat';
		$data['head'] = $this->load->view('head',$data);
		$this->load->view('pencapaian');
		
	}
	
	function ngo()
	{
		$data['title'] = 'Mitra Kami | Lifebuoy 10 Tahun Berbagi Sehat';
		$data['head'] = $this->load->view('head',$data);
		$this->load->view('ngo');
		
	}
	
	function submit_ide()
	{
		$data['title'] = 'Tulis Ide Kamu | Lifebuoy 10 Tahun Berbagi Sehat';
		$data['head'] = $this->load->view('head',$data);
		$this->load->view('submit_ide');
	}
        
        function galeri()
        {
            $datat['title'] = 'Gallery Ide Masyarakat | Lifebuoy 10 Tahun Berbagi Sehat';
            $datag['galery'] = $this->model_lifestoryide->ambilide(); 
            $head['head'] = $this->load->view('head',$datat);
            $this->load->view('galeri',$datag);
           
        }
	
	function submit_reg()
	{   
         $config = array(
                    array(
                          'field' =>'nama',
                          'label' =>'nama',
                          'rules' =>'required|min_length[5]|max_length[20]'
                           
                         ),
                    array(
                          'field' =>'jenis_kelamin',
                          'label' =>'jenis kelamin'
                           
                         ),
                    array(
                          'field' =>'email',
                          'label' =>'email',
                           'rules' =>'required'
                         ),
                    array(
                          'field' =>'tanggal',
                          'label' =>'tanggal',
                           'rules' =>'required|integer|min_length[2]|max_length[2]'
                         ),
                    array(
                          'field' =>'bulan',
                          'label' =>'bulan',
                           'rules' =>'required|integer|min_length[2]|max_length[2]'
                         ),
                    array(
                          'field' =>'tahun',
                          'label' =>'tahun',
                         'rules' =>'required|integer|min_length[4]|max_length[4]'
                         ),
                 
                    array(
                          'field' =>'kontak_id',
                          'label' =>'kontak id',
                          'rules' =>'required|min_length[3]|max_length[3]'
                         ),
                 
                    array(
                          'field' =>'nomor_handphone',
                          'label' =>'nomor handphone',
                         'rules' =>'required|integer|min_length[7]|max_length[15]'
                         )
                    
                );
                $this->form_validation->set_rules($config);
		$data = $this->input->post(array('nama','jenis_kelamin','email','tanggal','bulan','tahun','kontak_id','nomor_handphone'),true);
		if($this->form_validation->run() == FALSE){
                   
                    $this->session->set_flashdata('notif','Harap data diisi dengan lengkap dan benar!');
                    redirect('Lifestory/submit_ide/#registration');
                }
                else
                { 
                   $this->model_lifestory->simpan($data);  
                   $this->tulis_ide($data); //panggil fungsi terakhir untuk input dan passing ambil data inputannya,disini ambil nama saja
                   
                   //$this->tulis_ide_simpan($data);
                             
                }
              
              }
					
		//fungsi halaman berikut yaitu tulis_ide 
               function tulis_ide($data){
            
                         $datas['title'] = 'Tulis Ide Kamu | Lifebuoy 10 Tahun Berbagi Sehat';
                         $datas['head'] = $this->load->view('head',$datas);
                         $this->load->view('submit_ides',$data);
                       
              		}
        
     //fungsi tulis ide simpannya
        function tulis_ide_simpan(){
//              $config = array(
//                            array(
//                             'field' =>'objective',
//                             'label' =>'objective',
//                             'rules' =>'required|max_length[200]'
//                           
//                             ),
//                             array(
//                             'field' =>'lokasi',
//                             'label' =>'lokasi',
//                             'rules' =>'required'
//                            
//                             ),
//                            array(
//                             'field' =>'ide',
//                             'label' =>'ide',
//                             'rules' =>'required'
//                            )
//                            );
//                        $this->form_validation->set_rules($config);
                        $data = $this->input->post(array('nama','objective','lokasi','ide'));
                        $this->model_lifestoryide->simpan($data);
                        
                        redirect('lifestory/home');
                        
                        
//                if($this->form_validation->run() == FALSE){
//                    $this->session->set_flashdata('notif','Data harus diisi lengkap dan benar!');
//                    redirect('lifestory/submit_reg/#tulis_ide');
//                }
//                else
//                { 
//                
//                }
        }
        
        
        
        
}