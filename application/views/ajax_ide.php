<div class="i-form-content" id="tulis_ide">

    <div class="i-form-title">
	<p>Tuliskan ide cerita ide berbagi sehat kamu dibawah ini :</p>
	<p style="color:red;">
	</p>
							
     </div>
       <?php
	$message = $this->session->flashdata('notif');
	if($message){								
	echo '<p class="alert alert-danger" style="text-align:center;color:red;" >'.$message .'</p>';									
		}
       ?>								
	<form role="form" action="<?php echo site_url('lifestory/tulis_ide')?>" method="post">

		<div class="i-form-group i-objective">
			<label class="label" for="objective">Objective & Target</label>
			<textarea style="width: 474px;" rows="3" maxlength="200" name="objective" class="form-control" id="objective" placeholder="Tuliskan objective ide kamu disini(Contoh: Ingin membangun sarana cuci tangan untuk warga di komplek)" required="required"></textarea>
                         <br>
                       <font size="2" color="red">* maksimal 200 karakter</font>
		</div>

		<div class="i-form-group i-lokasi">
			<label class="label" for="lokasi">Lokasi Ide Berbagi Sehat</label>
			<input type="text" name="lokasi" class="i-input-text" style="width: 474px;" id="lokasi" placeholder="Tuliskan Dimana Ide Berbagi Sehat ini akan diwujudkan(Contoh : Komplek Perumahan Kelapa Gading, Jakarta Utara)" required="required">
		</div>					
							
		<div class="i-form-group i-ide">
			<label class="label" for="ide">Ide Lengkap</label>
			<textarea style="width: 474px;" rows="3" name="ide" class="i-input-text" id="ide" placeholder="Tuliskan secara lengkap ide Berbagi Sehat Kamu di sini"  required="required"></textarea>
                                                        
		</div>
			

		<button type="submit" class="i-btn-submit">Kirim</button>
	</form>


</div>