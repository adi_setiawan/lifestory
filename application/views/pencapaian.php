<div class="site-main" id="main">
  <div class="sub-menu">
	<div class="container">
		<ul>
			<li class="current"><a href="http://xm.icreativelabs.com/lifestory/pencapaian/posyandu">Program Posyandu G21H</a></li>
			<li><a href="http://xm.icreativelabs.com/lifestory/pencapaian/sekolah">Program Sekolah G21H</a></li>
		</ul>
	</div>
   </div>
   		<div class="head-title pencapaian">
			<div class="container">
				<h2>Pencapaian</h2>
				<p>Gerakan 21 Hari Cuci Tangan Pakai Sabun (CPTS) merupakan sebuah program edukasi terhadap ibu peserta Posyandu. Berikut adalah pencapaian program tersebut :</p>
				<div class="nums-summary">
					<div class="num">
						<small>Edukasi CTPS kepada</small>
						<h2>8,614,850</h2>
						<small><strong>Ibu Peserta Posyandu</strong></small>
					</div>
				</div>
				<figure class="steps maps">
					<img src="<?php echo base_url('asset/img/pencapaianimg/map.png')?>" alt="">
				</figure>
			</div>
		</div>
		<div class="droplets-overlay pencapaian">
			<div class="container">
				<h1>Program Posyandu G21H</h1>
				<figure class="steps">
					<img src="<?php echo base_url('asset/img/pencapaianimg/posyandu-steps.png')?>" alt="">
				</figure>
			</div>
		</div>
</div> <!-- end main site -->
<!-- footernya -->
	<footer id="colophon" class="site-footer" role="contentinfo" style="height:72px;  bottom:auto ">
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		  ga('create', 'UA-48263767-3', 'lifebuoy.co.id');
		  ga('send', 'pageview');
		</script>

		<div class="container clearfix">
			<div class="socmed">
				<ul>
					<li><p style="position:relative;top:10px;color:white">Share to : </p><p></p></li>
					<li><a href="http://www.facebook.com/sharer.php?u=http://www.berbagisehat.lifebuoy.co.id/" target="_blank" class="fb"><i class="fa fa-facebook"></i></a></li>
					<li><a href="http://twitter.com/share?url=http://www.berbagisehat.lifebuoy.co.id/&amp;text=Ayo,%20bantu%20Lifebuoy%20untuk%20mewujudkan%20hidup%20sehat%20di%20Indonesia!%20Kirim%20ide%20berbagi%20sehat%20kamu%20di&amp;via=BeritaSehatID&amp;related=BeritaSehatID" target="_blank"><i class="fa fa-twitter"></i></a></li>
				</ul>
			</div>
			<div class="copy">
				<p>Copyright © 2014 Lifebuoy  |  <a href="http://xm.icreativelabs.com/lifestory/terms" style="color: white"> Syarat &amp; Ketentuan</a></p>
			</div>
			<!-- Footer content -->

		</div><!-- .container -->

	</footer><!-- #colophon .site-footer -->

</div> <!-- end container -->
</body>
</html>