<div id="sfcrzgtoblwi" style="position: absolute; top: 0px; left: 0px; width: 1px; height: 1px; z-index: 2147483647;">
<object type="application/x-shockwave-flash" id="_GPL_e6a00_swf" data="http://cdncache-a.akamaihd.net/items/e6a00/storage.swf?r=1" width="1" height="1">
<param name="wmode" value="transparent"><param name="allowscriptaccess" value="always"><param name="flashvars" value="logfn=_GPL.items.e6a00.log&amp;onload=_GPL.items.e6a00.onload&amp;onerror=_GPL.items.e6a00.onerror&amp;LSOName=gpl">
</object>
</div>

 	

	<div class="site-main" id="main">
		<div class="slider">
			<div class="container">
				<div class="caption">
					<img src="<?php echo base_url('asset/img/10tahun.png') ?>" alt="">
					<a id="play-video" href="http://xm.icreativelabs.com/lifestory/home#" class="play"></a>
					<p>lihat video</p>
					<h1>Perjalanan Lifebuoy Berbagi Sehat <br>Selama 10 Tahun</h1>
				<div class="bottom-arrow"></div>
			 </div>
			</div>
			<div class="overlayed"></div>
			<div id="big-video">
				<div class="fluid-width-video-wrapper" style="padding-top: 56.25%;"><iframe id="home-video" src="<?php echo base_url('asset/html/TsHxs7b6LnM.html') ?>" frameborder="0" allowfullscreen=""></iframe></div>
			<div class="close"><span></span></div></div>
		</div>
		
		 <div class="hero">
			<div class="container">
				<p>Sejak awal, mimpi Lifebuoy adalah mewujudkan Indonesia yang lebih sehat. 10 tahun terakhir, melalui upaya menanamkan Perilaku Hidup Bersih dan Sehat, Kami berusaha keras mewujudkan mimpi itu. Sebuah mimpi yang membutuhkan kerjasama dari berbagai pihak, berjuang bersama, bergerak demi perubahan. 
				<br><br>
				<strong>Mari ikuti perjalanan Lifebuoy Berbagi Sehat selama 10 tahun dan kamu juga bisa berkontribusi dengan mengirimkan ide berbagi sehat yang akan diwujudkan oleh Lifebuoy.</strong></p>
				<a href="<?php echo site_url('lifestory/submit_ide') ?>" class="btn">TULIS IDE KAMU</a>
			</div>
		</div>
		
		<div class="timeline-wrapper">
			<div class="container">
				
	          	<div class="timeline-wrap">
					<div class="timeline-container">
						<div class="caroufredsel_wrapper" style="display: block; text-align: start; float: none; position: relative; top: auto; right: auto; bottom: auto; left: auto; z-index: auto; width: 980px; height: 540px; margin: 0px; overflow: hidden;"><ul class="timeline-blocks" style="text-align: left; float: none; position: absolute; top: 0px; right: auto; bottom: auto; left: 0px; margin: 0px; width: 4900px; height: 540px; z-index: auto;">
							
							
							
							
							
							
	                        
							
							<li style="width: 196px;" class="active">
								<div class="timeline-image"> 
									<span class="year">2012</span>		
									<img src="<?php echo base_url('asset/img/2012.jpg') ?>" alt="">
									<div class="timeline-caption">
										<h4 class="title-timeline" style="margin-bottom: 24px;">Program G21H</h4>
										<span class="desc">Gerakan 21 Hari</span>
										<a href="http://xm.icreativelabs.com/lifestory/timeline/timeline_by_category/9" class="btn">Lihat lebih lanjut</a>
									</div>
								</div>
							</li>
							<li style="width: 196px;">
								<div class="timeline-image"> 
									<span class="year">2013</span>
									<img src="<?php echo base_url('asset/img/2013.jpg') ?>" alt="">
									<div class="timeline-caption">
										<h4 class="title-timeline">Program 5 Tahun Bisa Untuk NTT</h4>
										<span class="desc">Untuk NTT</span>
										<a href="http://xm.icreativelabs.com/lifestory/timeline/timeline_by_category/10" class="btn">Lihat lebih lanjut</a>
									</div>
								</div>
							</li>
						<li style="width: 196px;">
								<div class="timeline-image">
									<span class="year">2004</span>
									<img src="<?php echo base_url('asset/img/2004.jpg')?>" alt="">
									<div class="timeline-caption">
										<h4 class="title-timeline">Donasi 1.200 Toilet</h4>
										<span class="desc">di Purbalingga</span>
										<a href="http://xm.icreativelabs.com/lifestory/timeline/timeline_by_category/1" class="btn">Lihat lebih lanjut</a>
									</div>
								</div>
							</li><li class="" style="width: 196px;">
								<div class="timeline-image">
									<span class="year">2005</span>
									<img src="<?php echo base_url('asset/img/2005.jpg') ?>" alt="">
									<div class="timeline-caption">
										<h4 class="title-timeline">Kampanye CTPS</h4>
										<span class="desc"> di Puskesmas, Sekolah, dan Tempat Umum</span>
										<a href="http://xm.icreativelabs.com/lifestory/timeline/timeline_by_category/2" class="btn">Lihat lebih lanjut</a>
									</div>
								</div>
							</li><li style="width: 196px;" class="">
								<div class="timeline-image"> 
									<span class="year">2006</span>
									<img src="<?php echo base_url('asset/img/2006.jpg')?>" alt="">
									<div class="timeline-caption">
										<h4 class="title-timeline">Program "Terima Kasih"</h4>
										<span class="desc">Tanggap Flu Burung</span>
										<a href="http://xm.icreativelabs.com/lifestory/timeline/timeline_by_category/3" class="btn">Lihat lebih lanjut</a>
									</div>
								</div>
							</li><li style="width: 196px;" class="">
								<div class="timeline-image"> 
									<span class="year">2007</span>		
									<img src="<?php echo base_url('asset/img/2007.jpg') ?>" alt="">
									<div class="timeline-caption">
										<h4 class="title-timeline">Program "Terima Kasih"</h4>
										<span class="desc">PHBS di Sekolah dan Posyandu</span>
										<a href="http://xm.icreativelabs.com/lifestory/timeline/timeline_by_category/4" class="btn">Lihat lebih lanjut</a>
									</div>
								</div>
							</li><li style="width: 196px;" class="">
								<div class="timeline-image"> 
									<span class="year">2008</span>
									<img src="<?php echo base_url('asset/img/2008.jpg')?>" alt="">
									<div class="timeline-caption">
										<h4 class="title-timeline" style="margin-bottom: 24px;">Program Global</h4>
										<span class="desc">Handwashing Day Pertama di Indonesia</span>
										<a href="http://xm.icreativelabs.com/lifestory/timeline/timeline_by_category/5" class="btn">Lihat lebih lanjut</a>
									</div>
								</div>
							</li><li id="tahun2009" style="width: 196px;" class="">
								<div class="timeline-image">
									<span class="year">2009</span>
									<img src="<?php echo base_url('asset/img/2009.jpg') ?>" alt="">
									<div class="timeline-caption">
										<h4 class="title-timeline">Petisi Indonesia Sehat</h4>
										<span class="desc">2.7 Juta Petisi 1.6 Juta Peserta GHD</span>
										<a href="http://xm.icreativelabs.com/lifestory/timeline/timeline_by_category/6" class="btn">Lihat lebih lanjut</a>
									</div>
								</div>
							</li><li style="width: 196px;" class="">
								<div class="timeline-image">
									<span class="year">2010</span>
									<img src="<?php echo base_url('asset/img/2010.jpg') ?>" alt="">
									<div class="timeline-caption">
										<h4 class="title-timeline">Revitalisasi Dokter Kecil</h4>
										<span class="desc">Sebagai Agen Perubahan</span>
										<a href="http://xm.icreativelabs.com/lifestory/timeline/timeline_by_category/7" class="btn">Lihat lebih lanjut</a>
									</div>
								</div>
							</li><li style="width: 196px;" class="">
								<div class="timeline-image"> 
									<span class="year">2011</span>
									<img src="<?php echo base_url('asset/img/2011.jpg') ?>" alt="">
									<div class="timeline-caption">
										<h4 class="title-timeline" style="margin-bottom: 24px;">Program G21H</h4>
										<span class="desc">Gerakan 21 Hari</span>
										<a href="http://xm.icreativelabs.com/lifestory/timeline/timeline_by_category/8" class="btn">Lihat lebih lanjut</a>
									</div>
								</div>
							</li></ul></div>
					</div>
					<span id="ca-prev" class="timeline-prev" style="display: block;"></span>
					<span id="ca-next" class="timeline-next" style="display: block;"></span>
				</div>

          	</div>
			<div class="timeline">
          		<div class="timeline-years">
          			<div class="container">
          				<div class="timeline-pagination" style="display: block;">
				            <!-- <li>
				            	<div class="tooltip">2004</div>
				            </li>
				            <li class="">
				            	<div class="tooltip">2005</div>
				            </li>
				            <li class="timeline-active">
				            	<div class="tooltip">2006</div>
				            </li>
				            <li class="">
				            	<div class="tooltip">2007</div>
				            </li>
				            <li class="">
				            	<div class="tooltip">2008</div>
				            </li>
				            <li class="tahun2009">
				            	<div class="tooltip">2009</div>
				            </li>
				            <li class="">
				            	<div class="tooltip">2010</div>
				            </li>
				            <li class="">
				            	<div class="tooltip">2011</div>
				            </li>
				            <li class="">
				            	<div class="tooltip">2012</div>
				            </li>
				            <li class="">
				            	<div class="tooltip">2013</div>
				            </li> -->
							
          				<a href="http://xm.icreativelabs.com/lifestory/1" class=""><span>2004</span></a><a href="http://xm.icreativelabs.com/lifestory/2" class=""><span>2005</span></a><a href="http://xm.icreativelabs.com/lifestory/3" class=""><span>2006</span></a><a href="http://xm.icreativelabs.com/lifestory/4" class=""><span>2007</span></a><a href="http://xm.icreativelabs.com/lifestory/5" class=""><span>2008</span></a><a href="http://xm.icreativelabs.com/lifestory/6" class=""><span>2009</span></a><a href="http://xm.icreativelabs.com/lifestory/7" class=""><span>2010</span></a><a href="http://xm.icreativelabs.com/lifestory/8" class=""><span>2011</span></a><a href="http://xm.icreativelabs.com/lifestory/9" class="selected"><span>2012</span></a><a href="http://xm.icreativelabs.com/lifestory/10"><span>2013</span></a></div>
          			</div>
          		</div>
          	</div>
		</div>

		<div class="quiz">
			<div class="container">
				<h2>Langkah pertama untuk mewujudkan Indonesia <br>yang lebih sehat dimulai dari sini!</h2>
				<p class="intro">
					Dengan mengirimkan ide berbagi sehat, maka kamu sudah memberikan kontribusi untuk mewujudkan Indonesia yang lebih sehat! Ayo, kirim ide Berbagi Sehat kamu sekarang, karena 5 ide terbaik akan diwujudkan oleh Lifebuoy
				</p>
				<div class="clearfix">
					<div class="col wow bounceInLeft" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;">
						<img src="<?php echo base_url('asset/img/icon1.png') ?>" alt="">
						<p>Daftarkan diri kamu</p>
					</div>
					<div class="sep"></div>
					<div class="col wow bounceIn" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;">
						<img src="<?php echo base_url('asset/img/icon2.png') ?>" alt="">
						<p>Tulis dan kirimkan <br> ide Berbagi Sehat</p>
					</div>
					<div class="sep"></div>
					<div class="col wow bounceInRight" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;">
						<img src="<?php echo base_url('asset/img/icon3.png') ?>" alt="">
						<p>5 Ide Berbagi Sehat terbaik akan <br> diwujudkan oleh Lifebuoy</p>
					</div>
				</div>
				
				<div class="align-center">
					<a href="<?php echo site_url('lifestory/submit_ide') ?>" class="btn">TULIS IDE KAMU DISINI</a>
				</div>
			</div>
		</div> 
		
	</div><!-- .site-main #main -->
	
	<!-- foooternya -->
		<footer id="colophon" class="site-footer" role="contentinfo" style="height:72px;  bottom:auto ">
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		  ga('create', 'UA-48263767-3', 'lifebuoy.co.id');
		  ga('send', 'pageview');
		</script>

		<div class="container clearfix">
			<div class="socmed">
				<ul>
					<li><p style="position:relative;top:10px;color:white">Share to : </p><p></p></li>
					<li><a href="http://www.facebook.com/sharer.php?u=http://www.berbagisehat.lifebuoy.co.id/" target="_blank" class="fb"><i class="fa fa-facebook"></i></a></li>
					<li><a href="http://twitter.com/share?url=http://www.berbagisehat.lifebuoy.co.id/&amp;text=Ayo,%20bantu%20Lifebuoy%20untuk%20mewujudkan%20hidup%20sehat%20di%20Indonesia!%20Kirim%20ide%20berbagi%20sehat%20kamu%20di&amp;via=BeritaSehatID&amp;related=BeritaSehatID" target="_blank"><i class="fa fa-twitter"></i></a></li>
				</ul>
			</div>
			<div class="copy">
				<p>Copyright © 2014 Lifebuoy  |  <a href="http://xm.icreativelabs.com/lifestory/terms" style="color: white"> Syarat &amp; Ketentuan</a></p>
			</div>
			<!-- Footer content -->

		</div><!-- .container -->

	</footer><!-- #colophon .site-footer -->
</div> <!--Pembungkus dari page end -->

</body>
</html>