var $ = jQuery.noConflict();
$(document).ready(function(){
	
	// FiTVIDS
	$("#big-video, .video-big").fitVids();


	// TIMELINE
	// $('.timeline li').on('click', showTimelineBlock);
	// $('.timeline-prev').on('click', prevTimeline);
	// $('.timeline-next').on('click', nextTimeline);

	// function showTimelineBlock() {
	//   var dataClass = $(this).attr('data-class');
	//   var listNumber = $('.timeline li').index(this);
	//   var left = listNumber * -212;
	//   if(dataClass < 2010){
	//   	$('.timeline-blocks').animate({ marginLeft: left + 'px'}, 500);
	//   } else {
	//   	console.log('tee');
	//   	$('.timeline-blocks').animate({ marginLeft:  "-1060px"}, 500);
	//   }
	  
	//   $(".timeline li").removeClass("timeline-active");
	//   $(this).addClass('timeline-active');
	//   $('.timeline-image').removeClass("active");
	//   $(".image-"+ dataClass).addClass("active");
	//   var bigImage = $(".image-"+$(this).css('marginLeft'));
	//   $('.timeline-blocks').attr('data-left',left);

	// };

	// function getPositionSlider(nav){
	// 	var left = $('.timeline-blocks').attr('style');
	// 	// margin-left:0px; 
	// 	var res = left.replace("margin-left:","");
	// 	res = res.replace("px;","");
	// 	res = $.trim(res);

		

	// 	if(nav == 'prev'){
	// 		res = parseInt(res) + 212;
	// 	}else{
	// 		res = parseInt(res) - 212;
	// 	}

	// 	console.log(res);

	// 	if(res <= 0 && res > -1061){
	// 		console.log(res);
	// 		return true;
	// 	} else {
	// 		return false;
	// 	}
	// }

	// function nextTimeline () {
	// 	if(getPositionSlider('next') == true){
	// 		$('.timeline-active').removeClass('timeline-active').next().addClass('timeline-active');  
	//  		$('.timeline-blocks').animate({ marginLeft: "+=-212"}, 700);
	// 	}
	// };

	// function prevTimeline () {
	// 	if(getPositionSlider('prev') == true){
	// 		  $('.timeline-active').removeClass('timeline-active').prev().addClass('timeline-active');
	// 		  $('.timeline-blocks').animate({ marginLeft: "+=212"}, 700);
	// 	}
	// };

	$('.timeline-blocks').carouFredSel({
        responsive 		: true, 
        width			: "100%",
        circular		: true,
		infinite		: false,
		auto    		: false,
		// transition		: true,
		scroll 			: {
			items        : 1,
			pauseOnHover: true
		},
        items 			: {
        	visible     :5,
        	width       : 212,
        	height      : 540,
        },
        prev    : {
		        button  : "#ca-prev",
		        key     : "left",
		        onBefore: function( data ) {
					unhighlight( data.items.old );
				},
				onAfter	: function( data ) {
					highlight( data.items.visible );
				}
		},
		next    : {
		        button  : "#ca-next",
		        key     : "right",
		        onBefore: function( data ) {
					unhighlight( data.items.old );
				},
				onAfter	: function( data ) {
					highlight( data.items.visible );
				}
		},
		pagination  : {
			container:".timeline-pagination",
			anchorBuilder:function( nr ) {
	            var title = $(this).find( ".year" ).text();
	            return "<a href="+nr+"><span>" + title + "</span></a>";
	        }

		},
		auto	: {
			onBefore: function( data ) {
				unhighlight( data.items.old );
			},
			onAfter	: function( data ) {
				highlight( data.items.visible );
			}
		}
                
    });
    function highlight( items ) {
		items.filter(":first-child").addClass("active");
		return items;
	}
	function unhighlight( items ) {
		items.removeClass("active");
		return items;
	}


	highlight( unhighlight( $("#foo3 > *") ) );

    $('.thumbnail-carousel').carouFredSel({
        responsive 		: true,
        // width			: "100%",
        circular		: false,
		infinite		: false,
		auto    		: false,
		scroll 			: 1,
		pagination  : {
			container:".timeline-pagination",
			anchorBuilder:function( nr ) {
				alert($(this));
	            var title = $(this).find( ".year" ).text();
	            return "<a href="+nr+"><span>" + title + "</span></a>";
	        }

		},
                
    });
    // $(".video-thumbnail .cols").click(function(){
    	// $(".video-mode").fadeIn();
    // })
    $(".video-close").click(function(e){
    	e.preventDefault();
    	$(".video-mode").fadeOut();
    })

    $(window).load(function(){
	    var container = document.querySelector('.idea-container');
		var msnry = new Masonry( container, {
		  // options
		  // columnWidth: 200,
		  itemSelector: '.idea',
		  // gutter:20
		});
    	
    })

	// PLAY OVELAYED VIDEO
	$('.play').click(function(event) {
		$('.caption').fadeOut(400);
		$('.overlayed').fadeOut(400);
	});
	


	//ADDING ELEMENT FOR MOBILE
	$('.site-navigation').append('<a href="#" class="mobile"><span></span></a>');
	$('.mobile').click(function(event) {
		$(this).toggleClass('mobile-toggle');
		$('.site-navigation').toggleClass('nav-toggle')
	});
 	

 	//SET DOCUMENT HEIGHT AS CONTENT HEIGHT
 	// var tinggi = $(document).height();
  //   $(".site").css('height',tinggi+'px');



 	//VIDEO CLOSE BTN
    $('.video-big, #big-video').append('<div class="close"><span></span></div>');
    $('.caption').append('<div class="bottom-arrow"></div>');

	$('.close').click(function() {
		$('.caption').fadeIn(400);
		$('.overlayed').fadeIn(400);
	});
    // BUTTON DOWN ARROW ON CLICK
	$('.bottom-arrow').click(function(){
		$.scrollTo( '.timeline-wrapper', 800, {easing:'easeOutQuad'} );
	});

	$('.btn.tulis-ide').click(function(){
		$.scrollTo( '.ide-form', 800, {easing:'easeOutQuad'} );
	});



	// IF SCROLL TO THE BOTTOM OF THE PAGE
	$( document ).scroll(function() {
		var scroll = $(document).scrollTop();
		var limit = $(document).height() - $(window).height() ;

		console.log(scroll);
		console.log(limit);
		if( scroll < limit ){
			$('.horizontal').css({
				bottom : 0
			});
		} else {
			$('.horizontal').css({
				bottom : 70
			});
			
		}
	});

	$(".cols").click(function(e){
		e.preventDefault();
		var id= $(this).attr("id");
		$(".details."+id).fadeIn();
	});
	$(".details-close").click(function(e){
		e.preventDefault();
		$(".details").fadeOut();
	})

	$(window).scroll(function () {

	     if ( $(window).scrollTop() > 300) {
		      //code goes here...
		    $(".timeline-wrapper.p10th").addClass("fixed");
		    $(".video-thumbnail").css("margin-top",60);
		 }else{
		 	$(".timeline-wrapper.p10th").removeClass("fixed");
		 	$(".video-thumbnail").css("margin-top",0);

		 }
	});


	$(document).imagesLoaded(function(){
		// Hanya untuk method slider & tabs
	});

});


