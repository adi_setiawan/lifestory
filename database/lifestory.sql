-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2016 at 07:11 AM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lifestory`
--

-- --------------------------------------------------------

--
-- Table structure for table `datapribadi`
--

CREATE TABLE IF NOT EXISTS `datapribadi` (
`id` int(8) NOT NULL,
  `nama` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `jenis_kelamin` char(20) COLLATE latin1_general_ci NOT NULL,
  `tanggal` int(8) NOT NULL,
  `bulan` int(8) NOT NULL,
  `tahun` int(8) NOT NULL,
  `email` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `kontak_id` int(8) NOT NULL,
  `nomor_handphone` int(8) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `datapribadi`
--

INSERT INTO `datapribadi` (`id`, `nama`, `jenis_kelamin`, `tanggal`, `bulan`, `tahun`, `email`, `kontak_id`, `nomor_handphone`) VALUES
(1, 'tes', 'Laki-laki', 1, 12, 1990, 'tes@gmail.com', 61, 562347757),
(2, 'irman hasko', 'Laki-laki', 20, 9, 1999, 'ir@yahoo.co.id', 61, 567834456),
(7, 'tes', 'Laki-laki', 4, 6, 1990, 'suka@gmail.com', 61, 84753945),
(10, 'tes sayakeberapa', 'Laki-laki', 11, 11, 1988, 'suka@gmail.com', 61, 53234567);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `datapribadi`
--
ALTER TABLE `datapribadi`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `datapribadi`
--
ALTER TABLE `datapribadi`
MODIFY `id` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
